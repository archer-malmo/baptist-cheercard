﻿$(function () {
    var textCount = function (e) {
        window.textBoxTest = e.target;
        var messageLength = e.target.value.length;
        var maxLength = e.target.maxLength;
        if (messageLength > maxLength) {
            e.target.value = e.target.value.substring(0, maxLength);
            e.target.blur();
            e.target.focus();
            return false;
        } else {
            $("#personal-message-limit").html((maxLength - messageLength) + " characters remaining");
        }
    };

    $("#personalMessage").keyup(textCount);
    $("#personalMessage").blur(textCount);

    $("#goBack").click(function () {
        window.history.back(-1);
    });

    $.validator.addMethod("pattern", function (value, element) {
        return this.optional(element) || RegExp(element.pattern).test(value);
    });

    if ($("#card-form").length > 0) {
        $("#card-form").validate();

        $("#phoneNumber").rules("add", {
            pattern: true,
            messages: {
                pattern: "This field requires a valid telephone number."
            }
        });
    }

    $("#sendCard").click(function (e) {
        if ($("#sendCard").hasClass("disabled")) {
            e.preventDefault();
            return false;
        } else {
            $("#sendCard").addClass("disabled");
        }
    });

    $("a, input[type='submit']").click(function () {
        window.parent.postMessage(["linkClick", true], "*");
    });
});