﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BaptistCheerCardApplication.Models;
using BaptistCheerCardApplication.Utils;

namespace BaptistCheerCardApplication.Controllers
{
    public class CategoriesController : Controller
    {
        private CardRetriever db = RetrieverInit.baptistDb;

        // GET: Categories
        public ActionResult Index()
        {
            var categoryList = db.GetCategoriesList();
            return View(categoryList.ToList());
        }

        // GET: Categories/Index/5
    }
}
