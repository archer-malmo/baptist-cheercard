﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaptistCheerCardApplication.Models;
using BaptistCheerCardApplication.Utils;
using System.Net;
using System.Configuration;

namespace BaptistCheerCardApplication.Controllers
{
    public class HomeController : Controller
    {
        private CardRetriever db = RetrieverInit.baptistDb;

        public ActionResult Index()
        {
            ViewBag.RootDomain = ConfigurationManager.AppSettings["apiRootDomain"];
            return View(db.GetCategoriesList().ToList());
        }

        public ActionResult Category(string id)
        {
            if(id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IEnumerable<Card> cardList = db.GetCardsList(id);
            ViewBag.RootDomain = ConfigurationManager.AppSettings["apiRootDomain"];
            return View(cardList.ToList());
        }

        public ActionResult Customize(string id)
        {
            if(id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Card currentCard = db.GetCardByID(id);
            ViewBag.RootDomain = ConfigurationManager.AppSettings["apiRootDomain"];
            CheerCardConfig emailListConfig = db.GetCheerCardConfig();
            List<string> sendToNames = emailListConfig.EmailList.Keys.ToList();
            ViewBag.Locations = sendToNames;
            return View(currentCard);
        }

        public ActionResult Preview() {
            Card currentCard = db.GetCardByID(Request.Form["cardId"]);
            // Store form data in session
            var current = HttpContext.Session;
            foreach(var key in Request.Form.AllKeys) {
                current[key] = Request.Form[key];
            }
            ViewBag.RootDomain = ConfigurationManager.AppSettings["apiRootDomain"];
            return View(currentCard);
        }

        public ActionResult Confirmation() {
            var current = HttpContext.Session;
            Card currentCard = db.GetCardByID((string) current["cardId"]);
            PdfSender pdfGenerator = new PdfSender(current, HttpContext.Server.MapPath("~"));
            if ((string) current["alreadySent"] != "true") {
                // Build list of emails to send to
                CheerCardConfig emailListConfig = db.GetCheerCardConfig();
                List<string> sendToEmails = new List<string>();
                // Add location email
                string currentLocation = (string)current["hospitalLocation"];
                if (emailListConfig.EmailList.ContainsKey(currentLocation)) {
                    sendToEmails.Add(emailListConfig.EmailList[currentLocation]);
                }
                // Add sender's email if they chose to receive a copy
                if ((string)current["sendCopy"] == "true") {
                    sendToEmails.Add((string)current["emailAddress"]);
                }
                pdfGenerator.SendPdf(sendToEmails, currentCard);
                current["alreadySent"] = "true";
            }
            current["pdfLinkUrl"] = String.Format("{0}://{1}/{2}", Request.Url.Scheme, Request.Url.Host, pdfGenerator.PdfBaseName);

            ViewBag.RootDomain = ConfigurationManager.AppSettings["apiRootDomain"];
            return View(currentCard);
        }
    }
}