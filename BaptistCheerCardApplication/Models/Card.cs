﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BaptistCheerCardApplication.Models
{
    public class Card
    {
        public string ItemID;
        public string ItemName;
        public string ItemPath;
        public string ParentID;
        public string TemplateID;
        public string TemplateName;
        public string CloneSource;
        public string ItemLanguage;
        public string ItemVersion;
        public string DisplayName;
        public string HasChildren;
        public string ItemIcon;
        public string ItemMedialUrl;
        public string ItemUrl;
        public string CardImage;
        public string Name;

        private CardImage _cardImageObject;

        public CardImage CardImageObject {
            get {
                if(_cardImageObject == null) {
                    _cardImageObject = RetrieverInit.baptistDb.GetCardImageByID(getCardImageId());
                }
                return _cardImageObject;
            }
        }

        private string getCardImageId() {
            XElement cardImageElement = XElement.Parse(this.CardImage);
            return cardImageElement.Attribute("mediaid").Value.Trim(new char[] { '{', '}' });
        }
    }
}