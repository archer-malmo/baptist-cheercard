﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaptistCheerCardApplication.Models {
    public class CheerCardConfig {
        public string ItemID;
        public string ItemName;
        public string ItemPath;
        public string ParentID;
        public string TemplateID;
        public string TemplateName;
        public string CloneSource;
        public string ItemLanguage;
        public string ItemVersion;
        public string DisplayName;
        public string HasChildren;
        public string ItemIcon;
        public string ItemMedialUrl;
        public string ItemUrl;
        public string SendToEmails;

        private Dictionary<string, string> _emailList;

        public Dictionary<string, string> EmailList {
            get {
                if(_emailList == null) {
                    _emailList = new Dictionary<string, string>();
                    // Split by ampersand first
                    string[] ampSplit = this.SendToEmails.Split('&');
                    foreach(string record in ampSplit) {
                        // Split by equal sign
                        string[] recordSplit = record.Split('=');
                        string name = recordSplit[0];
                        string email = recordSplit[1];
                        _emailList[name] = HttpUtility.UrlDecode(email);
                    }
                }
                return _emailList;
            }
        }
    }
}