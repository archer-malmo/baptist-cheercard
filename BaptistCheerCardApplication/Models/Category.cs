﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaptistCheerCardApplication.Models
{
    public class Category
    {
        public string ItemID;
        public string ItemName;
        public string ItemPath;
        public string ParentID;
        public string TemplateID;
        public string TemplateName;
        public string CloneSource;
        public string ItemLanguage;
        public string ItemVersion;
        public string DisplayName;
        public string HasChildren;
        public string ItemIcon;
        public string ItemMedialUrl;
        public string ItemUrl;
        public string CategoryName;

        private IEnumerable<Card> _cards;

        public IEnumerable<Card> Cards {
            get {
                if (_cards == null) {
                    _cards = RetrieverInit.baptistDb.GetCardsList(this.ItemID);
                }
                return _cards;
            }
        }


        public Card GetRandomCard() {
            Random rand = new Random();
            int randomId = rand.Next(Cards.ToList().Count);
            if(randomId >= Cards.ToList().Count) {
                return null;
            }
            return Cards.ToList()[randomId];
        }
    }
}