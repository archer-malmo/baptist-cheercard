﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaptistCheerCardApplication.Models {
    public class CardImage {
        public string ItemID;
        public string ItemName;
        public string ItemPath;
        public string ParentID;
        public string TemplateID;
        public string TemplateName;
        public string CloneSource;
        public string ItemLanguage;
        public string ItemVersion;
        public string DisplayName;
        public string HasChildren;
        public string ItemIcon;
        public string ItemMedialUrl;
        public string ItemUrl;
        public string Keywords;
        public string Asset3;
        public string DateTime;
        public string Dimensions;
        public string MimeType;
        public string Alt;
        public string Latitude;
        public string Asset2;
        public string ZipCode;
        public string Make;
        public string FilePath;
        public string Blob;
        public string Width;
        public string Software;
        public string Extension;
        public string Marketingasset;
        public string Size;
        public string Title;
        public string LocationDescription;
        public string Longitude;
        public string Height;
        public string Format;
        public string Model;
        public string Copyright;
        public string ImageDescription;
        public string Asset1;
        public string Artist;
        public string Description;
        public string CountryCode;
    }
}