﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.IO;

namespace BaptistCheerCardApplication {
    public static class PdfCleaner {
        private static Dictionary<DateTime, string> fileQueue = new Dictionary<DateTime, string>();
        private static Thread cleanerThread;

        private static void CleanFileQueue() {
            while(true) {
                try {
                    int queueCount;
                    lock(fileQueue) {
                        queueCount = fileQueue.Count;
                    }
                    if (queueCount > 0) {
                        DateTime now = DateTime.Now;
                        foreach (DateTime fileTimestamp in fileQueue.Keys.ToList()) {
                            if (now > fileTimestamp) {
                                lock (fileQueue) {
                                    string filename = fileQueue[fileTimestamp];
                                    if (File.Exists(filename)) {
                                        File.Delete(filename);
                                        fileQueue.Remove(fileTimestamp);
                                    }
                                }
                            }
                        }
                    } else {
                        Thread.Sleep(Timeout.Infinite);
                    }
                } catch(ThreadInterruptedException interruptedException) {
                    continue;
                } catch(ThreadAbortException abortException) {
                    return;
                }
            }
        }

        public static bool StartCleanerThread() {
            cleanerThread = new Thread(new ThreadStart(CleanFileQueue));
            cleanerThread.IsBackground = true;
            cleanerThread.Start();
            if (cleanerThread.ThreadState == ThreadState.Running || cleanerThread.ThreadState == ThreadState.WaitSleepJoin) {
                return true;
            } else {
                return false;
            }
        }

        public static void AddFileToQueue(DateTime timestamp, string filePath) {
            lock (fileQueue) {
                fileQueue.Add(timestamp, filePath);
            }
            if (cleanerThread.ThreadState == (ThreadState.WaitSleepJoin | ThreadState.Background)) {
                cleanerThread.Interrupt();
            }
        }

        public static void StopCleanerThread() {
            cleanerThread.Abort();
        }
    }
}