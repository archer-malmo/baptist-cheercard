﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.IO;
using Newtonsoft.Json;
using BaptistCheerCardApplication.Models;
using System.Threading.Tasks;
using System.Configuration;

namespace BaptistCheerCardApplication.Utils {
    public class CardRetriever {
        private const string ROOT_FOLDER_ID = "544C77E1-440A-4DAE-B25D-E209D4CE51C1";
        private const string API_ENDPOINT = "/sitecore/api/ssc";

        protected string domain;
        protected string username;
        protected string password;

        private HttpClientHandler retrieverHandler;
        private HttpClient retrieverClient;
        private bool isLoggedIn;
        private string rootDomain;

        public CardRetriever() {
            this.domain = "sitecore";
            this.username = "cheercard";
            this.password = "do$L>{TkeBGOYz";
            this.rootDomain = ConfigurationManager.AppSettings["apiRootDomain"];
            this.retrieverHandler = new HttpClientHandler();
            this.retrieverHandler.CookieContainer = new CookieContainer();
            this.retrieverClient = new HttpClient(this.retrieverHandler);
            this.retrieverClient.BaseAddress = new Uri("https://" + rootDomain);
            this.login();
        }

        private void checkCookieExpiration() {
            CookieCollection currentCookies = this.retrieverHandler.CookieContainer.GetCookies(new Uri("https://" + rootDomain));
            if(currentCookies[".ASPXAUTH"] == null || currentCookies[".ASPXAUTH"].Expired) {
                this.isLoggedIn = false;
            }
        }

        private void login()
        {
            Dictionary<string, string> loginCredentials = new Dictionary<string, string>
            {
                { "domain", domain },
                { "username", username },
                { "password", password }
            };
            string loginJson = JsonConvert.SerializeObject(loginCredentials);
            HttpContent loginContent = new StringContent(loginJson, System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage loginResp = retrieverClient.PostAsync(API_ENDPOINT + "/auth/login", loginContent).GetAwaiter().GetResult();
            if(loginResp.StatusCode == HttpStatusCode.OK) {
                this.isLoggedIn = true;
            } else if(loginResp.StatusCode == HttpStatusCode.Forbidden) {
                this.isLoggedIn = false;
            }
        }

        public IEnumerable<Category> GetCategoriesList() {
            this.checkCookieExpiration();
            if (this.isLoggedIn) {
                UriBuilder categoryUriBuilder = new UriBuilder("https://" + this.rootDomain);
                categoryUriBuilder.Path = String.Format("{0}/item/{1}/children", API_ENDPOINT, ROOT_FOLDER_ID);
                HttpResponseMessage categoriesResp = retrieverClient.GetAsync(categoryUriBuilder.Uri).GetAwaiter().GetResult();
                if (categoriesResp.StatusCode == HttpStatusCode.OK) {
                    string categoriesJson = categoriesResp.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    IEnumerable<Category> categoryList = JsonConvert.DeserializeObject<List<Category>>(categoriesJson);
                    return categoryList;
                } else {
                    throw new Exception("Could not retrieve categories");
                }
            } else {
                this.login();
                if (this.isLoggedIn) {
                    return this.GetCategoriesList();
                } else {
                    throw new Exception("Could not log in to retrieve categories");
                }
            }
        }

        public IEnumerable<Card> GetCardsList(string categoryId) {
            this.checkCookieExpiration();
            if (this.isLoggedIn) {
                UriBuilder cardUriBuilder = new UriBuilder("https://" + this.rootDomain);
                string formattedCategoryId = categoryId.Trim(new char[] { '{', '}' });
                cardUriBuilder.Path = String.Format("{0}/item/{1}/children", API_ENDPOINT, formattedCategoryId);
                HttpResponseMessage cardsResp = retrieverClient.GetAsync(cardUriBuilder.Uri).GetAwaiter().GetResult();
                if (cardsResp.StatusCode == HttpStatusCode.OK) {
                    string cardsJson = cardsResp.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    IEnumerable<Card> cardList = JsonConvert.DeserializeObject<List<Card>>(cardsJson);
                    return cardList;
                } else {
                    throw new Exception("Could not retrieve cards");
                }
            } else {
                this.login();
                if (this.isLoggedIn) {
                    return this.GetCardsList(categoryId);
                } else {
                    throw new Exception("Could not log in to retrieve cards");
                }
            }
        }

        public Card GetCardByID(string cardId) {
            this.checkCookieExpiration();
            if (this.isLoggedIn) {
                UriBuilder cardUriBuilder = new UriBuilder("https://" + this.rootDomain);
                string formattedCardId = cardId.Trim(new char[] { '{', '}' });
                cardUriBuilder.Path = String.Format("{0}/item/{1}", API_ENDPOINT, formattedCardId);
                HttpResponseMessage cardResp = retrieverClient.GetAsync(cardUriBuilder.Uri).GetAwaiter().GetResult();
                if(cardResp.StatusCode == HttpStatusCode.OK) {
                    string cardJson = cardResp.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    Card card = JsonConvert.DeserializeObject<Card>(cardJson);
                    return card;
                } else {
                    throw new Exception("Could not retrieve card");
                }
            } else {
                this.login();
                if (this.isLoggedIn) {
                    return this.GetCardByID(cardId);
                } else {
                    throw new Exception("Could not log in to retrieve card");
                }
            }
        }

        public CardImage GetCardImageByID(string cardImageId) {
            this.checkCookieExpiration();
            if (this.isLoggedIn) {
                UriBuilder cardImageUriBuilder = new UriBuilder("https://" + this.rootDomain);
                string formattedCardImageId = cardImageId.Trim(new char[] { '{', '}' });
                cardImageUriBuilder.Path = String.Format("{0}/item/{1}", API_ENDPOINT, formattedCardImageId);
                HttpResponseMessage cardImageResp = retrieverClient.GetAsync(cardImageUriBuilder.Uri).GetAwaiter().GetResult();
                if (cardImageResp.StatusCode == HttpStatusCode.OK) {
                    string cardImageJson = cardImageResp.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    CardImage cardImage = JsonConvert.DeserializeObject<CardImage>(cardImageJson);
                    return cardImage;
                } else {
                    throw new Exception("Could not retrieve card image");
                }
            } else {
                this.login();
                if (this.isLoggedIn) {
                    return this.GetCardImageByID(cardImageId);
                } else {
                    throw new Exception("Could not log in to retrieve card image");
                }
            }
        }

        public CheerCardConfig GetCheerCardConfig() {
            this.checkCookieExpiration();
            if (this.isLoggedIn) {
                UriBuilder configUriBuilder = new UriBuilder("https://" + this.rootDomain);
                configUriBuilder.Path = String.Format("{0}/item/{1}", API_ENDPOINT, ROOT_FOLDER_ID);
                HttpResponseMessage configResp = retrieverClient.GetAsync(configUriBuilder.Uri).GetAwaiter().GetResult();
                if (configResp.StatusCode == HttpStatusCode.OK) {
                    string configJson = configResp.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    CheerCardConfig config = JsonConvert.DeserializeObject<CheerCardConfig>(configJson);
                    return config;
                }
                else {
                    throw new Exception("Could not retrieve cheer card configuration");
                }
            }
            else {
                this.login();
                if (this.isLoggedIn) {
                    return this.GetCheerCardConfig();
                }
                else {
                    throw new Exception("Could not log in to retrieve cheer card configuration");
                }
            }
        }
    }
}