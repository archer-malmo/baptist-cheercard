﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Net.Http;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
using System.Configuration;
using BaptistCheerCardApplication.Models;

namespace BaptistCheerCardApplication.Utils {
    public class PdfSender {
        private static readonly log4net.ILog log
       = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string SenderEmail;
        private string PdfFileName;
        private string AppPath;
        public string PdfBaseName { get; set; }
        private Dictionary<string, string> FormInfo;

        public PdfSender(HttpSessionStateBase sessionInfo, string applicationPath) {
            AppPath = applicationPath;
            PdfBaseName = "generated_pdfs\\" + String.Format("{0}{1}-{2}.pdf", sessionInfo["patientFirstName"], sessionInfo["patientLastName"], (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);
            PdfFileName = applicationPath + PdfBaseName;
            SenderEmail = (string) sessionInfo["emailAddress"];
            FormInfo = new Dictionary<string, string>();
            foreach (string sessionKey in sessionInfo.Keys) {
                FormInfo[sessionKey] = (string)sessionInfo[sessionKey];
            }
        }

        private void DrawPdf(Card cheerCardObj) {
            string rootDomain = ConfigurationManager.AppSettings["apiRootDomain"];
            // Create the PDF document
            log.Debug("Creating PDF...");
            Document cheerCardDoc = new Document();

            // Set the global font
            cheerCardDoc.Styles["Normal"].Font.Name = "Arial";

            // Create main section
            Section mainSection = cheerCardDoc.AddSection();
            mainSection.PageSetup.LeftMargin = "1cm";
            mainSection.PageSetup.RightMargin = "1cm";

            // Draw the header text
            Paragraph headerParagraph = mainSection.AddParagraph();
            Style centerStyle = cheerCardDoc.Styles.AddStyle("CenteredHeader", "Normal");
            centerStyle.ParagraphFormat.Alignment = ParagraphAlignment.Center;
            centerStyle.Font.Size = 24;
            headerParagraph.Style = "CenteredHeader";
            headerParagraph.AddFormattedText(String.Format("Hello, {0} {1}", FormInfo["patientFirstName"], FormInfo["patientLastName"]), TextFormat.Bold);

            // Draw the body text underneath
            Paragraph bodyParagraph = mainSection.AddParagraph();
            Style bodyStyle = cheerCardDoc.Styles.AddStyle("BodyText", "Normal");
            bodyStyle.Font.Size = 14;
            bodyStyle.ParagraphFormat.SpaceBefore = "7.5mm";
            bodyStyle.ParagraphFormat.SpaceAfter = "5mm";
            bodyParagraph.Style = "BodyText";
            bodyParagraph.AddText(FormInfo["personalMessage"]);

            // Get card image
            log.Debug("Getting image from Sitecore");
            CardImage cardImage = cheerCardObj.CardImageObject;
            bool isValidHost = Uri.IsWellFormedUriString(cardImage.ItemMedialUrl, UriKind.Absolute);
            string cardImageUrl = "";
            if (isValidHost) {
                cardImageUrl = cardImage.ItemMedialUrl;
            }
            else {
                cardImageUrl = "https://" + rootDomain + cardImage.ItemMedialUrl;
            }

            // Strip off query string arguments
            UriBuilder cardImageUriObj = new UriBuilder(cardImageUrl);
            cardImageUriObj.Query = "bc=transparent&db=web";
            cardImageUrl = cardImageUriObj.ToString();

            // Download the image temporarily
            string TempFilename = String.Format("{1}/{0}.jpg", Guid.NewGuid().ToString(), AppPath);
            try {
                (new WebClient()).DownloadFile(cardImageUrl, TempFilename);
            } catch(Exception e) {
                log.Error(e);
            }

            // Next, draw the actual cheer card image
            Image cheerCard = mainSection.AddImage(TempFilename);
            cheerCard.Width = cheerCardDoc.DefaultPageSetup.PageWidth - new Unit(2, UnitType.Centimeter);
            cheerCard.LockAspectRatio = true;
            cheerCard.RelativeHorizontal = RelativeHorizontal.Margin;
            cheerCard.RelativeVertical = RelativeVertical.Line;
            cheerCard.Top = ShapePosition.Top;
            cheerCard.Left = ShapePosition.Left;

            // Add spacer
            Paragraph spacer = mainSection.AddParagraph();
            Style spacerStyle = cheerCardDoc.Styles.AddStyle("Spacer", "Normal");
            spacerStyle.Font.Size = 6;
            spacer.Style = "Spacer";
            spacer.AddText(" ");

            // Draw the location logo
            string logoPath = String.Format("{0}/{1}", AppPath, LocationLogo.GetPath(FormInfo["hospitalLocation"]));
            Image locationLogo = mainSection.AddImage(logoPath);
            locationLogo.Width = (cheerCardDoc.DefaultPageSetup.PageWidth - new Unit(2, UnitType.Centimeter)) / 2;
            locationLogo.LockAspectRatio = true;
            locationLogo.RelativeHorizontal = RelativeHorizontal.Margin;
            locationLogo.RelativeVertical = RelativeVertical.Line;
            locationLogo.Top = ShapePosition.Top;
            locationLogo.Left = ShapePosition.Left;


            // Draw the rest of the text
            Paragraph footerParagraph = mainSection.AddParagraph();
            footerParagraph.Style = "BodyText";
            StringBuilder footerBuilder = new StringBuilder();
            if(!FormInfo["firstName"].Equals("") || !FormInfo["lastName"].Equals("")) {
                footerBuilder.AppendFormat("From: {0} {1}\n", FormInfo["firstName"], FormInfo["lastName"]);
            }
            if(!FormInfo["emailAddress"].Equals("")) {
                footerBuilder.AppendFormat("Email: {0}\n", FormInfo["emailAddress"]);
            }
            if(!FormInfo["phoneNumber"].Equals("")) {
                footerBuilder.AppendFormat("Phone Number: {0} ({1})", FormInfo["phoneNumber"], FormInfo["phoneType"]);
            }
            footerParagraph.AddText(footerBuilder.ToString());

            // Save the document...
            const bool unicode = true;
            const PdfFontEmbedding always = PdfFontEmbedding.Always;
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode, always);
            pdfRenderer.Document = cheerCardDoc;
            pdfRenderer.RenderDocument();
            try {
                pdfRenderer.PdfDocument.Save(PdfFileName);
            } catch(Exception e) {
                log.Error(e);
            }

            // Delete the cheer card image
            File.Delete(TempFilename);
        }

        public void SendPdf(List<string> emails, Card cheerCard) {
            log.Debug("Starting PDF generation");
            DrawPdf(cheerCard);
            log.Debug("Ending PDF generation");
            PdfCleaner.AddFileToQueue(DateTime.Now.AddHours(1), PdfFileName);

            // Build the body of the email
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.AppendLine("Sender Name: " + FormInfo["firstName"] + " " + FormInfo["lastName"]);
            bodyBuilder.AppendLine("Sender Email Address: " + FormInfo["emailAddress"]);
            if (FormInfo["phoneNumber"] != "") {
                bodyBuilder.AppendLine("Sender Phone Number: " + FormInfo["phoneNumber"] + " (" + FormInfo["phoneType"] + ")");
            }
            bodyBuilder.AppendLine("");
            bodyBuilder.AppendLine("Patient Name: " + FormInfo["patientFirstName"] + " " + FormInfo["patientLastName"]);
            bodyBuilder.AppendLine("Patient Location: " + FormInfo["hospitalLocation"]);
            bodyBuilder.AppendLine("Patient Room Number: " + FormInfo["patientRoomNumber"]);

            // Variables for all emails
            string from = ConfigurationManager.AppSettings["fromAddress"];
            string subject = String.Format("Cheer Card for {0} {1}", FormInfo["patientFirstName"], FormInfo["patientLastName"]);
            SmtpClient client = new SmtpClient();
            foreach(string email in emails) {
                MailMessage cheerCardEmail = new MailMessage(from, email);
                cheerCardEmail.Subject = subject;
                cheerCardEmail.Body = bodyBuilder.ToString();

                // Open a file stream to attach
                FileStream PdfStream = new FileStream(PdfFileName, FileMode.Open);
                Attachment thePdf = new Attachment(PdfStream, PdfBaseName.Replace("generated_pdfs\\", ""), "application/pdf");
                cheerCardEmail.Attachments.Add(thePdf);
                log.Debug("Sending email!");
                try {
                    client.Send(cheerCardEmail);
                } catch(Exception e) {
                    log.Error(e);
                }
                PdfStream.Close();
            }
        }
    }
}