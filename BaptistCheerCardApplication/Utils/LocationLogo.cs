﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaptistCheerCardApplication.Utils {
    public class LocationLogo {
        public static string GetUrl(string location, string hostServer) {
            string formattedLocation = location.Replace("'", "").Replace(" ", "-");
            return String.Format("{0}/location_logos/branding-{1}.jpg", hostServer, formattedLocation);
        }

        public static string GetPath(string location) {
            string formattedLocation = location.Replace("'", "").Replace(" ", "-");
            return String.Format("location_logos/branding-{0}.jpg", formattedLocation);
        }
    }
}